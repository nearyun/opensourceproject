package com.free.overscroll;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class OverscrollImageView extends OverscrollContainer<ImageView> {
	private ImageView mImageView;

	public OverscrollImageView(Context context) {
		this(context, null);
	}

	public OverscrollImageView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public OverscrollImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected boolean canOverscrollAtStart() {
		return true;
	}

	@Override
	protected boolean canOverscrollAtEnd() {
		return true;
	}

	@Override
	protected OverscrollContainer.OverscrollDirection getOverscrollDirection() {
		return OverscrollContainer.OverscrollDirection.Horizontal;
	}

	@Override
	protected ImageView createOverscrollView() {
		if (mImageView == null)
			mImageView = new ImageView(getContext());
		return mImageView;
	}

}
