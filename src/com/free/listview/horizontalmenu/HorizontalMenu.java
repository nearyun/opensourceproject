//package com.free.listview.horizontalmenu;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import android.content.Context;
//
///**
// * 
// * @author baoyz
// * @date 2014-8-23
// * 
// */
//public class HorizontalMenu {
//
//	private Context mContext;
//	private List<HorizontalMenuItem> mItems;
//	private int mViewType;
//
//	public HorizontalMenu(Context context) {
//		mContext = context;
//		mItems = new ArrayList<HorizontalMenuItem>();
//	}
//
//	public Context getContext() {
//		return mContext;
//	}
//
//	public void addMenuItem(HorizontalMenuItem item) {
//		mItems.add(item);
//	}
//
//	public void removeMenuItem(HorizontalMenuItem item) {
//		mItems.remove(item);
//	}
//
//	public List<HorizontalMenuItem> getMenuItems() {
//		return mItems;
//	}
//
//	public HorizontalMenuItem getMenuItem(int index) {
//		return mItems.get(index);
//	}
//
//	public int getViewType() {
//		return mViewType;
//	}
//
//	public void setViewType(int viewType) {
//		this.mViewType = viewType;
//	}
//
//}
